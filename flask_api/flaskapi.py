from flask import Flask, render_template, jsonify
from subprocess import PIPE, run
import re
import json 
from flask.helpers import make_response
from kubernetes import client, config
from datetime import datetime, timezone
import requests
import subprocess
import operator


app = Flask(__name__)

intervals = (
    ('w', 604800),  # 60 * 60 * 24 * 7
    ('d', 86400),    # 60 * 60 * 24
    ('h', 3600),    # 60 * 60
    ('m', 60),
    ('s', 1),
    )

def age(kube_start_time, granularity=2):
    result = []
    now_time = datetime.now(timezone.utc)
    seconds = int((now_time - kube_start_time).total_seconds())
    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            if value == 1:
                name = name.rstrip('s')
            result.append("{}{}".format(value, name))
    return ''.join(result[:granularity])

def get_instances(cloud_provider):
    command = ['kubectl', 'get', 'nodes','-o', 'json']
    nodes_info = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    json_string = nodes_info.stdout
    nodes_info_json = json.loads(json_string)

    result = {"aws": [], "gcp": [], "azure": [], "openstack": []}

    for nodes in nodes_info_json['items']:
        instance_name = nodes['metadata']['name']
        public_ip_address = nodes['metadata']['annotations']['rke.cattle.io/external-ip']
        x = instance_name.split("-")
        temp = {}
        if x[1] == "openstack":
            region = "melbourne"
            az = "melbourne-qh2-uom"
        elif x[1] == "aws":
            region = 'ap-southeast-2'
            az = x[-2]
        elif x[1] == "gcp":
            region = 'australia-southeast1'
            az = x[-2]
        elif x[1] == 'azure':
            region = 'australiaeast'
            az = 'NA'
        else:
            region = ''
            az = ''
        temp["node_name"] = instance_name
        temp["public_ip"] = public_ip_address
        temp["region"] = region
        temp["availability_zone"] = az
        result[x[1]].append(temp)
    
    return result[cloud_provider]


def get_cost_best_provider():
    # Read Cost from utilprice.py script output
    with open('output_cost_all_providers.json') as json_file:
        final_output = json.load(json_file)
    return final_output

def best_provider():
    # Cheapest and most expensive
    cost_provider = get_cost_best_provider()
    # print(cost_provider)
    # openstack_price = float(1.00)
    aws_price = cost_provider['aws']
    azure_price = cost_provider['azure']
    openstack_price =  cost_provider['openstack']
    gcp_price = cost_provider['gcp']

    highest = max(aws_price, azure_price,openstack_price, gcp_price)
    cheapest = min(aws_price, azure_price,openstack_price, gcp_price)

    if (aws_price == cheapest):
        best = "aws"
        best_display = "AWS - Amazon Web Services"
    elif (azure_price == cheapest):
        best = "azure"
        best_display = "MS Azure - Microsoft Azure"
    elif (openstack_price == cheapest):
        best = "openstack"
        best_display = "UoM - Melbourne Research Cloud"
    else:
        best = "gcp"
        best_display = "GCP - Google Cloud Platform"
        
    final_output = {"best": best}
    return final_output

def pod_status_json():
    command = ['kubectl', 'get', 'pods','-o', 'json']
    pods_info = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    json_string = pods_info.stdout
    pods_info_json = json.loads(json_string)
    return pods_info_json

def node_status_json():
    command = ['kubectl', 'get', 'nodes','-o', 'json']
    nodes_info = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    json_string = nodes_info.stdout
    nodes_info_json = json.loads(json_string)
    return nodes_info_json

def pod_status_str():
    pods_status = subprocess.check_output(["kubectl", "get", "pods"])
    pod_status_str = str(pods_status)
    return pod_status_str


def nodes_info_str():
    nodes_info = subprocess.check_output(["kubectl","get","nodes"])
    nodes_info_str = str(nodes_info)
    return nodes_info_str

def scaleup():
    # pods_status = subprocess.check_output(["kubectl", "get", "pods"])
    # pod_status_str = str(pods_status)
    count = re.findall("Pending" , pod_status_str())
    if len(count) == 0:
        status = 'ok'
        az = ''
    else:
        status = 'up'

    best_cloud_provider = best_provider()
    # print(best_cloud_provider)
    provider = best_cloud_provider['best']
    # print(provider)
    # nodes_info = subprocess.check_output(["kubectl","get","nodes"])
    # nodes_info_str = str(nodes_info)

    if status == 'up':
        
        if provider == 'aws' or provider == 'gcp':
            nodes_a = re.findall("worker-" + provider + "-" + "a" + "-\d+", nodes_info_str())
            nodes_b = re.findall("worker-" + provider + "-" + "b" + "-\d+", nodes_info_str())
            nodes_c = re.findall("worker-" + provider + "-" + "c" + "-\d+", nodes_info_str())
            if (len(nodes_a)) > (len(nodes_b)):
                az = 'b'
            elif (len(nodes_b)) > (len(nodes_c)):
                az = 'c'
            else:
                az = 'a'
        else:
            az = 'default'        
        # print("Availability Zone: " + az)

    output_up = {"status": status, "provider": provider, "az": az}
    return output_up

def scaledown():
    worker_nodes = re.findall("worker-", nodes_info_str())
    number_worker_nodes = len(worker_nodes)
    print(number_worker_nodes)

    # count_pods = re.findall("deployment" , pod_status_str())
    pods_status_json = pod_status_json()
    number_pods = len(pods_status_json['items'])
    # print(number_pods)
    
    if 3*number_worker_nodes < number_pods:
        kube_state = "high"
        last_expensive_worker_node = "null"
    elif number_worker_nodes < number_pods <= 3*number_worker_nodes:
        kube_state = "normal"
        last_expensive_worker_node = "null"
    elif number_worker_nodes == 0:
        kube_state = "ok"
        last_expensive_worker_node = "null"
    else:
        kube_state = "down"
        # with open('output_all_providers.json') as json_data:
        #     providers = json.load(json_data)
        providers = get_cost_best_provider()
        # print(type(providers))
        sorted_providers = dict(sorted(providers.items(), key=operator.itemgetter(1), reverse=True))
        for key in sorted_providers.keys():
            expensive_worker_nodes = re.findall("worker-" + key + "-" + "." + "-\d+", nodes_info_str())
            if len(expensive_worker_nodes) > 0:
                sorted_expensive_worker_nodes = sorted(expensive_worker_nodes, reverse=True)
                last_expensive_worker_node = sorted_expensive_worker_nodes[0]
                break
    
    output_down = {"status": kube_state, "content": last_expensive_worker_node}
    return output_down

# @app.route('/')
@app.route('/aws/list_instance', methods=['GET'])
def get_aws():
    resp = make_response(jsonify(get_instances('aws')))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp
    # return jsonify(get_instances('aws'))

@app.route('/gcp/list_instance', methods=['GET'])
def get_gcp():
    resp = make_response(jsonify(get_instances('gcp')))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp
    # return jsonify(get_instances('gcp'))

@app.route('/azure/list_instance', methods=['GET'])
def get_azure():
    resp = make_response(jsonify(get_instances('azure')))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp
    # return jsonify(get_instances('azure'))

@app.route('/openstack/list_instance', methods=['GET'])
def get_openstack():
    resp = make_response(jsonify(get_instances('openstack')))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp
    # return jsonify(get_instances('openstack'))

@app.route('/kube/get_pods', methods=['GET'])
def get_pods():
    config.load_kube_config()
    v1 = client.CoreV1Api()
    # print("Listing pods with their IPs:")
    result_pods = v1.list_namespaced_pod("default", pretty='pretty', watch='false')
    pod_status = {'pod_details': []}
    for each_pod in result_pods.items:
        temp = {} 
        status = each_pod.status.phase
        if status != 'Running':
            ready = '0/1'
        else:
            ready = '1/1'
        # print("%s\t%s\t%s\t%s\t%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name, ready, i.status.phase, age(i.status.start_time), i.spec.node_name))
        temp['pod_name'] = each_pod.metadata.name
        temp['ready'] = ready
        temp['status'] = each_pod.status.phase
        temp['node_name'] = each_pod.spec.node_name
        temp['age'] = age(each_pod.status.start_time)
        temp['namespace'] = each_pod.metadata.namespace
        temp['pod_internal_ip'] = each_pod.status.pod_ip
        pod_status['pod_details'].append(temp)

    resp = make_response(jsonify(pod_status))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/kube/get_deployments', methods=['GET'])
def get_deployments():
    config.load_kube_config()
    appsV1 = client.AppsV1Api()
    result_deployments = appsV1.list_namespaced_deployment("default", pretty='pretty', watch='false')
    
    deployment_status = {'deployment_details': []}

    for deployments in result_deployments.items:
        temp = {} 
        status_replicas = deployments.status.replicas
        ready_replicas = deployments.status.ready_replicas
        temp['pod_name'] = deployments.metadata.name
        temp['age'] = age(deployments.metadata.creation_timestamp)
        temp['namespace'] = deployments.metadata.namespace
        temp['ready'] = str(ready_replicas)+"/"+str(status_replicas)
        temp['available'] = deployments.status.available_replicas
        deployment_status['deployment_details'].append(temp)
    resp = make_response(jsonify(deployment_status))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/monitoring/costprovider', methods=['GET'])
def cost_provider():
    resp = make_response(jsonify(get_cost_best_provider()))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/monitoring/bestprovider', methods=['GET'])
def best_cloud_provider():
    resp = make_response(jsonify(best_provider()))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/monitoring/scaleup', methods=['GET'])
def scale_up():
    resp = make_response(jsonify(scaleup()))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/monitoring/scaledown', methods=['GET'])
def scale_down():
    resp = make_response(jsonify(scaledown()))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)


# @app.route('/', methods=['GET', 'POST'])
# @app.route("/aws")
# def home():
#     return render_template('home.html')


# @app.route("/about")
# def about():
#     return render_template('about.html')
