## To build docker image for flask
- Dockerfile is already present. Make sure you switch to directory where dockerfile is present and run the below command. 
```
docker build -t team09kubernetes/flaskapi:v5 .
```
- To push the image to dockerhub. Make sure to login to your dockerhub account `docker login --username team09kubernetes`

```
docker push team09kubernetes/flaskapi:v5
```

## Steps to run the flaskapi.py

1. Make sure the kube_config is in the home directory of the user. Copy the kube_config to $HOME/.kube/  
```
mkdir -p $HOME/.kube/ && \
cp kube_config $HOME/.kube/
```
### On Docker:
Run the flaskapi in docker:
```
docker rm -f flaskapi; docker run --name flaskapi --restart unless-stopped -v $HOME/.kube:/root/.kube -p 5000:5000 -d  team09kubernetes/flaskapi:v5
```

### On Command line:
- First install pip requirements
```
sudo  /usr/bin/python3 -m pip install -r requirements.txt
```
- Run the flaskapi.py
```
python3 flaskapi.py
```


## Endpoints

curl -I http://localhost:5000/aws/list_instance
curl -I http://localhost:5000/gcp/list_instance
curl -I http://localhost:5000/azure/list_instance
curl -I http://localhost:5000/openstack/list_instance
curl -I http://localhost:5000/kube/get_pods
curl -I http://localhost:5000/kube/get_deployments
curl -I http://localhost:5000/monitoring/costprovider
curl -I http://localhost:5000/monitoring/bestprovider
curl -I http://localhost:5000/monitoring/scaleup
curl -I http://localhost:5000/monitoring/scaledown
curl -I http://localhost:5000/monitoring/bestprovider


curl -I http://115.146.94.151:5000/aws/list_instance
curl -I http://115.146.94.151:5000/gcp/list_instance
curl -I http://115.146.94.151:5000/azure/list_instance
curl -I http://115.146.94.151:5000/openstack/list_instance
curl -I http://115.146.94.151:5000/kube/get_pods
curl -I http://115.146.94.151:5000/kube/get_deployments
curl -I http://115.146.94.151:5000/monitoring/costprovider
curl -I http://115.146.94.151:5000/monitoring/bestprovider
curl -I http://115.146.94.151:5000/monitoring/scaleup
curl -I http://115.146.94.151:5000/monitoring/scaledown