#!/usr/bin/env python
# coding: utf-8

import requests
import json

# Retrieve AWS Price
aws = requests.get("https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/ap-southeast-2/index.json")
# with open('price_list_aws_provider.json') as json_file:
#     aws_json = json.load(json_file)
aws_json = aws.json()


sku = "7NYHPHSMD45SYSNN"

for product in aws_json["terms"]["OnDemand"]:
    for offer in aws_json["terms"]["OnDemand"][product]:
        if sku == aws_json["terms"]["OnDemand"][product][offer]["sku"]:
            for code in aws_json["terms"]["OnDemand"][product][offer]["priceDimensions"]:
                aws_price = aws_json["terms"]["OnDemand"][product][offer]["priceDimensions"][code]["pricePerUnit"]["USD"]
                aws_price = float(aws_price)
                print("AWS Price: " + "USD " + str(aws_price))
                break
            break
        break



# Retrieve MS Azure Price

url = "https://login.microsoftonline.com/4c3c549d-44bd-4d73-9962-156af6108587/oauth2/token"
payload = 'client_id=835579fc-1f2d-499c-9c05-52701921a7d1&client_secret=0c963d5a-2c9f-4986-826c-e2542183ed4f&resource=https%3A//management.core.windows.net/&grant_type=client_credentials'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Cookie': 'esctx=AQABAAAAAAAm-06blBE1TpVMil8KPQ41_kB54Kvfgmm0RCIS-ThkVVhvmKq7eUOsZevUl3WO3FZG6NNHI6O3CyIhY_UdrXsAHT8LkfvHjpk6Ob1nwYYEXykPv1qpWAHFJ8VxqmhX9l5WraX0QECJlrHvakTo8-a68ACwWdXQT4fSVJnNFVgrZ4Pwi9POOSgF4cCRq_X_JUQgAA; x-ms-gateway-slice=prod; stsservicecookie=ests; fpc=AvQnVhBahFFFrtfAwshH1iFZ31q0AQAAAG6nNNYOAAAA'
}
response = requests.request("POST", url, headers=headers, data = payload)
#print(response.text.encode('utf8'))
output_azure = response.json()
token = str(output_azure['access_token'])


url = "https://management.azure.com/subscriptions/31e79a12-dcf9-4698-8e82-14fe250ce442/providers/Microsoft.Commerce/RateCard?api-version=2016-08-31-preview&%24filter=OfferDurableId+eq+'MS-AZR-0003P'+and+Currency+eq+'USD'+and+Locale+eq+'en-US'+and+RegionInfo+eq+'US'"

payload = {}
headers = {
  'Authorization': 'Bearer' + ' ' + token
}

response = requests.request("GET", url, headers=headers, data = payload)
# with open('price_list_azure_provider.json') as json_file:
#     azure_json = json.load(json_file)

azure_json = response.json()




MeterId = "f4d7a5a5-1b67-45ea-b1a0-282fbdd34b05"

for product in azure_json["Meters"]:
    if MeterId == product["MeterId"]:
        MeterRate = product["MeterRates"]['0']
        azure_price = float(MeterRate)
        print("MS Azure Price: " + "USD " + str(azure_price))




# Retrieve GCP Price

gcp = requests.get("https://cloudbilling.googleapis.com/v1/services/6F81-5844-456A/skus?key=AIzaSyCMnc1RO-od4NQv0sJe2hAS9t_Q_QAXCHM")
# with open('price_list_gcp_provider.json') as json_file:
#     gcp_json = json.load(json_file)

gcp_json = gcp.json()



# Core
skuId_1 = "1BE3-7C1B-4E3E"

# RAM
skuId_2 = "2E28-1B91-432D"

# IP
skuId_3 = "4AF8-7C1F-39C4"

for sku in gcp_json["skus"]:
    if skuId_1 == sku["skuId"]:
        for summary in sku["pricingInfo"]:
            for price in summary["pricingExpression"]["tieredRates"]:
                gcp_price_1 = price["unitPrice"]['nanos']
                gcp_price_1 = float(gcp_price_1/1000000000)
                
    elif skuId_2 == sku["skuId"]:
        for summary in sku["pricingInfo"]:
            for price in summary["pricingExpression"]["tieredRates"]:
                gcp_price_2 = price["unitPrice"]['nanos']
                gcp_price_2 = float(gcp_price_2/1000000000)
                
    elif skuId_3 == sku["skuId"]:
        for summary in sku["pricingInfo"]:
            for price in summary["pricingExpression"]["tieredRates"]:
                gcp_price_3 = price["unitPrice"]['nanos']
                gcp_price_3 = float(gcp_price_3/1000000000)
                

gcp_price = gcp_price_1 + gcp_price_2 + gcp_price_3
print("GCP Price: " + "USD " + str(gcp_price))



# Cheapest and most expensive

openstack_price = float(1.00)
highest = max(gcp_price, azure_price, openstack_price, aws_price)
cheapest = min(gcp_price, azure_price, openstack_price, aws_price)



aws_output       = "AWS Price:                  " + "USD " + "{:2.4f}".format(aws_price)
azure_output     = "MS Azure Price:             " + "USD " + "{:2.4f}".format(azure_price)
openstack_output = "UoM Research Cloud Price:   " + "USD " + "{:2.4f}".format(openstack_price)
gcp_output       = "GCP Price:                  " + "USD " + "{:2.4f}".format(gcp_price)


print("Instances prices: ")
print("======================================")
if (aws_price == highest):
    print(aws_output + " <-- Highest")
elif (aws_price == cheapest):
    print(aws_output + " <-- Lowest")
else:
    print(aws_output)
if (azure_price == highest):
    print(azure_output + " <-- Highest")
elif (azure_price == cheapest):
    print(azure_output + " <-- Lowest")
else:
    print(azure_output)
if (openstack_price == highest):
    print(openstack_output + " <-- Highest")
elif (openstack_price == cheapest):
    print(openstack_output + " <-- Lowest")
else:
    print(openstack_output)
if (gcp_price == highest):
    print(gcp_output + " <-- Highest")
elif (gcp_price == cheapest):
    print(gcp_output + " <-- Lowest")
else:
    print(gcp_output)
print("======================================")
# print("=====================================\n")
# print("Remaining Resources: ")
# print("Threshold: 5")
# print("\n=====================================")


# Write Prices to file

with open('output_all_providers.txt', 'w') as outfile:
        outfile.write("Instances prices: \n")
        outfile.write("======================================\n")
        if (aws_price == highest):
            outfile.write(aws_output + " <-- Highest\n")
        elif (aws_price == cheapest):
            outfile.write(aws_output + " <-- Lowest\n")
        else:
            outfile.write(aws_output + "\n")
        if (azure_price == highest):
            outfile.write(azure_output + " <-- Highest\n")
        elif (azure_price == cheapest):
            outfile.write(azure_output + " <-- Lowest\n")
        else:
            outfile.write(azure_output + "\n")
        if (openstack_price == highest):
            outfile.write(openstack_output + " <-- Highest\n")
        elif (openstack_price == cheapest):
            outfile.write(openstack_output + " <-- Lowest\n")
        else:
            outfile.write(openstack_output + "\n")
        if (gcp_price == highest):
            outfile.write(gcp_output + " <-- Highest\n")
        elif (gcp_price == cheapest):
            outfile.write(gcp_output + " <-- Lowest\n")
        else:
            outfile.write(gcp_output + "\n")
        outfile.write("======================================")
#         outfile.write("=====================================\n")
#         outfile.write("Remaining Resources: \n")
#         outfile.write("Threshold: 5\n")
#         outfile.write("=====================================")

if (aws_price == cheapest):
    best = "aws"
    best_display = "AWS - Amazon Web Services"
elif (azure_price == cheapest):
    best = "azure"
    best_display = "MS Azure - Microsoft Azure"
elif (openstack_price == cheapest):
    best = "openstack"
    best_display = "UoM - Melbourne Research Cloud"
else:
    best = "gcp"
    best_display = "GCP - Google Cloud Platform"
    
best_text = "Current Best Cloud Resourse For Scaling Up: \n" + best_display
print(best_text)
with open('output_best.txt', 'w') as outfile:
            outfile.write(best_text)

# Write output to file

# output = {'provider': best}
# output_util_price = json.dumps(output)

output = {"aws": aws_price, "azure": azure_price, "openstack": openstack_price, "gcp": gcp_price, "best": best}

with open('output_all_providers.json', 'w') as outfile:
    json.dump(output, outfile)

# with open('output_util_price.txt', 'w') as outfile:
#             outfile.write(best)

