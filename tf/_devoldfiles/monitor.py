#!/usr/bin/env python
# coding: utf-8


import re
# import pandas as pd
# import numpy as np
import json
import subprocess
import operator
from pandas.compat import StringIO



# Set Pandas DataFrame

# pd.set_option('display.max_rows', 500)
# pd.set_option('display.max_columns', 500)
# pd.set_option('display.width', 1000)


# Check Kubernetes Pods' status

pods_status = subprocess.check_output(["kubectl", "get", "pods"])
# pods_status = pd.read_csv('pods_status.txt', delim_whitespace=True)
pod_stastus_str = str(pods_status)
# print(pods_status)
count = re.findall("Pending" , pod_stastus_str)



# count = pods_status['STATUS'] == 'Pending'

# print(count)

if len(count) == 0:
    status = 'ok'
    az = ''
else:
    status = 'up'

status


# Retrieve best cloud provider

with open('output_util_price.txt') as file:
    provider = file.read()
print(provider)



# Check Kubernetes Nodes' information and define Availability Zone (AZ)

nodes_info = subprocess.check_output(["kubectl","get","nodes"])
    # with open('nodes_status.txt') as file:
    #     nodes_info = file.read()
nodes_info_str = str(nodes_info)

if status == 'up':
    
    if provider == 'aws' or provider == 'gcp':
        nodes_a = re.findall("worker-" + provider + "-" + "a" + "-\d+", nodes_info_str)
        nodes_b = re.findall("worker-" + provider + "-" + "b" + "-\d+", nodes_info_str)
        nodes_c = re.findall("worker-" + provider + "-" + "c" + "-\d+", nodes_info_str)
        if (len(nodes_a)) > (len(nodes_b)):
            az = 'b'
        elif (len(nodes_b)) > (len(nodes_c)):
            az = 'c'
        else:
            az = 'a'
    else:
        az = 'default'        
    # print(nodes_a)
    # print(nodes_b)
    # print(nodes_c)
    print("Availability Zone: " + az)

output_up = {"status": status, "provider": provider, "az": az}


with open('scaleup.json', 'w') as outfile:
    json.dump(output_up, outfile)


worker_nodes = re.findall("worker-", nodes_info_str)
number_worker_nodes = len(worker_nodes)

# number_pods = len(pods_status['STATUS'])


# number_pods = subprocess.check_output(["kubectl", "get", "pods"])
# number_pods = str(number_pods)

# df = pd.read_csv(StringIO(number_pods))
# print(df)

# number_pods = len(df['STATUS'])
# print(type(df))


count_pods = re.findall("deployment" , pod_stastus_str)
number_pods = len(count_pods)
print(number_pods)

if 3*number_worker_nodes < number_pods:
    kube_state = "high"
    last_expensive_worker_node = ""
elif number_worker_nodes < number_pods <= 3*number_worker_nodes:
    kube_state = "normal"
    last_expensive_worker_node = ""
else:
    kube_state = "down"
    with open('output_all_providers.json') as json_data:
        providers = json.load(json_data)
    sorted_providers = dict(sorted(providers.items(), key=operator.itemgetter(1), reverse=True))
    for key in sorted_providers.keys():
        expensive_worker_nodes = re.findall("worker-" + key + "-" + "." + "-\d+", nodes_info_str)
        if len(expensive_worker_nodes) > 0:
            sorted_expensive_worker_nodes = sorted(expensive_worker_nodes, reverse=True)
            last_expensive_worker_node = sorted_expensive_worker_nodes[0]
            break


output_down = {"status": kube_state, "content": last_expensive_worker_node}



with open('scaledown.json', 'w') as outfile:
    json.dump(output_down, outfile)



#TEST!!!

# with open('output_all_providers.json') as json_data:
#     providers = json.load(json_data)
# sorted_providers = dict(sorted(providers.items(), key=operator.itemgetter(1), reverse=True))
# print(sorted_providers)


# for key in sorted_providers.keys():
#     expensive_worker_nodes = re.findall("worker-" + key + "-" + "." + "-\d+", nodes_info)
#     if len(expensive_worker_nodes) > 0:
#         sorted_expensive_worker_nodes = sorted(expensive_worker_nodes, reverse=True)
#         last_expensive_worker_node = sorted_expensive_worker_nodes[0]
#         break
# print(last_expensive_worker_node)



# expensive_worker_nodes = re.findall("worker-" + key + "-" + "." + "-\d+", nodes_info)
# number_worker_nodes = len(worker_nodes)
# sorted_nodes = sorted(expensive_worker_nodes)
# sorted_nodes



### ------> Here Starts the process of printing the cluster status 
## Think do we need to show this, if yes, how ? 

with open('scaleup.json') as json_data:
    response_scaleup = json.load(json_data)



print("Current Cluster Status:")
    
if (response_scaleup["status"] == "ok"):
    print("No Need to Scale Up")
    with open('text_server.txt', 'w') as outfile:
        outfile.write("Current Cluster Status:\n")
        outfile.write("No Need to Scale Up\n")
    
    # Scale down
    
    with open('scaledown.json') as json_file:
        scaling_down = json.load(json_file)

    if (scaling_down["status"] == "ok" or scaling_down["status"] == "normal" or scaling_down["status"] == "high"):
        print("No Need to Scale Down")
        with open('text_server.txt', 'w') as outfile:
            outfile.write("Current Cluster Status:\n")
            outfile.write("No Need to Scale Up\n")
            outfile.write("No Need to Scale Down")
            
    elif(scaling_down["status"] == "down"):
        print("Ready to Scale Down: " + scaling_down["content"])
        with open('text_server.txt', 'w') as outfile:
            outfile.write("Current Cluster Status:\n")
            outfile.write("No Need to Scale Up\n")
            outfile.write("Ready to Scale Down: " + scaling_down["content"])
            
    else:
        print("Unexpected Output from Server")
        with open('text_server.txt', 'w') as outfile:
            outfile.write("Current Cluster Status:\n")
            outfile.write("No Need to Scale Up\n")
            outfile.write("Unexpected Output from Server")
        
elif(response_scaleup["status"] == "up"):
    print("Ready to Scale Up: " + response_scaleup["provider"] +"-"+ response_scaleup["az"])
    with open('text_server.txt', 'w') as outfile:
        outfile.write("Current Cluster Status: \n")
        outfile.write("Ready to Scale Up: " + response_scaleup["provider"] +"-"+ response_scaleup["az"])
        
else:
    print("Unexpected Output from Server")
    with open('text_server.txt', 'w') as outfile:
        outfile.write("Current Cluster Status: \n")
        outfile.write("Unexpected Output from Server")




