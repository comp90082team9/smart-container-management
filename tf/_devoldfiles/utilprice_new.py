#!/usr/bin/env python
# coding: utf-8

import requests
import json

# Retrieve AWS Price
aws = requests.get("https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/ap-southeast-2/index.json")
aws_json = aws.json()

# storing json data into file
with open('price_list_aws_provider.json', 'w') as outfile:
    json.dump(aws_json, outfile)


# Retrieve MS Azure Price
access_token_url = "https://login.microsoftonline.com/4c3c549d-44bd-4d73-9962-156af6108587/oauth2/token"
payload = 'client_id=835579fc-1f2d-499c-9c05-52701921a7d1&client_secret=0c963d5a-2c9f-4986-826c-e2542183ed4f&resource=https%3A//management.core.windows.net/&grant_type=client_credentials'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Cookie': 'esctx=AQABAAAAAAAm-06blBE1TpVMil8KPQ41_kB54Kvfgmm0RCIS-ThkVVhvmKq7eUOsZevUl3WO3FZG6NNHI6O3CyIhY_UdrXsAHT8LkfvHjpk6Ob1nwYYEXykPv1qpWAHFJ8VxqmhX9l5WraX0QECJlrHvakTo8-a68ACwWdXQT4fSVJnNFVgrZ4Pwi9POOSgF4cCRq_X_JUQgAA; x-ms-gateway-slice=prod; stsservicecookie=ests; fpc=AvQnVhBahFFFrtfAwshH1iFZ31q0AQAAAG6nNNYOAAAA'
}
response = requests.request("POST", access_token_url, headers=headers, data = payload)
#print(response.text.encode('utf8'))
output_azure = response.json()
token = str(output_azure['access_token'])

azure_api_url = "https://management.azure.com/subscriptions/31e79a12-dcf9-4698-8e82-14fe250ce442/providers/Microsoft.Commerce/RateCard?api-version=2016-08-31-preview&%24filter=OfferDurableId+eq+'MS-AZR-0003P'+and+Currency+eq+'USD'+and+Locale+eq+'en-US'+and+RegionInfo+eq+'US'"
payload = {}
headers = {
  'Authorization': 'Bearer' + ' ' + token
}

response = requests.request("GET", azure_api_url, headers=headers, data = payload)
azure_json = response.json()

# storing json data into file
with open('price_list_azure_provider.json', 'w') as outfile:
    json.dump(azure_json, outfile)

# Retrieve GCP Price
gcp = requests.get("https://cloudbilling.googleapis.com/v1/services/6F81-5844-456A/skus?key=AIzaSyCMnc1RO-od4NQv0sJe2hAS9t_Q_QAXCHM")
gcp_json = gcp.json()

# storing json data into file
with open('price_list_gcp_provider.json', 'w') as outfile:
    json.dump(gcp_json, outfile)