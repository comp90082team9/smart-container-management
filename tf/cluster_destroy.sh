#!/bin/bash

cd scripts
jq '. + { "base_cluster_info_slaves_updated"} | ."base_cluster_info_slaves_updated"=."base_cluster_info"' ./base_cluster.tfvars.json > ./temp/temp1.tfvars.json
jq '. + { "extra_workers_updated"} | ."extra_workers_updated"=."extra_workers"' ./temp/temp1.tfvars.json > ./temp/temp.tfvars.json
terraform destroy -var-file=./auth.tfvars -var-file=./temp/temp.tfvars.json
exit 0