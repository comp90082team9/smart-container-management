locals {
  subnets = tomap({for n, n_info in var.instance_info : n => (n_info.role == "master" ? google_compute_subnetwork.master-subnet.name : google_compute_subnetwork.worker-subnet.name)})
  azs = tomap({for n, n_info in var.instance_info : n => (n_info.az == "a" ? data.google_compute_zones.available.names[0] : (n_info.az == "b" ? data.google_compute_zones.available.names[1] : data.google_compute_zones.available.names[2]))})
}

resource "google_compute_instance" "kubenode" {
  for_each = var.instance_info
  name         = each.key
  machine_type = "n1-standard-1"
  zone         = local.azs[each.key]

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  metadata = {
    ssh-keys = "ec2-user:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7PMtNCS8C+Hfec3LD7zovgdxSPrNIhqkiDSlaaCBaN8TaL8/8fzkOxTuzM7sUZ4ZqQmjLuJbRCzZyZrTOEVPO+c63UmL/hGxU97Qv8S5SFItmDG1ZBEQulMs4qTJ3neEzJkiNldyDnaV3sU5FN7OviT1BZiiYG30I6cUvvcQllw2ZZNwfckJiC6QXr7KtqgXcUrG+9Kk2EZmWpP8DJO7CeYdEVZQTRVotR47GnQD0jf0VNNF/E0UDfwocSs21Zt6xLC7P4E5flYbpKvNF0OVHEkiMvB/32Zb3JNFmuQ9NXqoJqu9eF4SYY61Wg7Bo5VCJs/zrxTs85w6OUzl1oJ3N suraj@surajthakur"
  }

  network_interface {
    # A default network  is created for all GCP projects
    subnetwork       = local.subnets[each.key]
    access_config {
        
    }
  }

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
        host     = self.network_interface.0.access_config.0.nat_ip
        user     = "ec2-user"
        private_key="${file("nectar_unimelb.pem")}"
      }

      inline = [
        "curl https://releases.rancher.com/install-docker/18.09.2.sh | sh",
        "sudo usermod -a -G docker ec2-user",
        "sudo mkdir -p /data",
        "sudo yum install -y nfs-utils",
        "sudo mount 115.146.94.139:/data /data" 
      ]
  }
}

output "instances" {
  value = tomap({for node, node_v in google_compute_instance.kubenode: node => node_v.network_interface.0.access_config.0.nat_ip})
}


