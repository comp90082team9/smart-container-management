provider "google" {
  credentials = "${file("My First Project-789b0ff153f4.json")}"
  project     = "vivid-alchemy-276100"
  region      = "australia-southeast1"
  zone        = "australia-southeast1-a"
}

variable "instance_info" {
  type = map
}

data "google_compute_zones" "available" {}

resource "google_compute_network" "vpc-master" {
  name          =  "kube-vpc-master"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
}

resource "google_compute_network" "vpc-worker" {
  name          =  "kube-vpc-worker"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
}

resource "google_compute_firewall" "kube-worker" {
  name    = "kube-worker"
  network = "${google_compute_network.vpc-worker.name}"

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443","2376", "10250","10254","1-65000"]
  }

    allow {
    protocol = "udp"
    ports    = ["8472", "1-65000"]
  }
}

resource "google_compute_firewall" "kube-master" {
  name    = "kube-master"
  network = "${google_compute_network.vpc-master.name}"

  allow {
    protocol = "tcp"
    ports    = ["2376", "2379", "2380", "9099","10250","80","443","6443","10254","1-65000","22"]
  }

    allow {
    protocol = "udp"
    ports    = ["8472", "1-65000"]
  }

}

resource "google_compute_subnetwork" "master-subnet" {
  name          = "master-subnet"
  ip_cidr_range = "10.2.0.0/16"
  network       = "${google_compute_network.vpc-master.name}"
  region        = "australia-southeast1"
}


resource "google_compute_subnetwork" "worker-subnet" {
  name          = "worker-subnet"
  ip_cidr_range = "10.2.0.0/16"
  network       = "${google_compute_network.vpc-worker.name}"
  region        = "australia-southeast1"
}
