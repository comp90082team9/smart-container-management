module "test-gcp" {
    source = "../"
    instance_info = tomap({
        "worker-gcp-a-1": {
            "role" : "worker",
            "az" : "a"
        }
    })
}

output "instances" {
  value = module.test-gcp.instances
}