resource "openstack_networking_secgroup_v2" "etcd" {
  name        = "kube_etcd"
  description = "etcd security group"
}

resource "openstack_networking_secgroup_v2" "controlplane" {
  name        = "kube_controlplane"
  description = "controlplane security group"
}

resource "openstack_networking_secgroup_v2" "worker" {
  name        = "kube_worker"
  description = "worker security group"
}

resource "openstack_networking_secgroup_v2" "ssh" {
  name        = "kube_ssh"
  description = "ssh security group"
}

resource "openstack_networking_secgroup_rule_v2" "ssh_rule_1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.ssh.id}"
}

resource "openstack_networking_secgroup_rule_v2" "ssh_rule_2" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.ssh.id}"
}
