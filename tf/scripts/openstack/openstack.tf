locals {
  security_groups = tomap({for n, n_info in var.instance_info : n => (n_info.role == "master" ? ["${openstack_networking_secgroup_v2.etcd.name}", "${openstack_networking_secgroup_v2.controlplane.name}", "${openstack_networking_secgroup_v2.ssh.name}"] : ["${openstack_networking_secgroup_v2.worker.name}"])})
}
resource "openstack_compute_instance_v2" "kubenode" {
  for_each        = var.instance_info
  name            = each.key
  image_id        = "bfc847f1-c711-4832-a825-f7e62868981b"
  flavor_id       = "5e433981-e5d4-4175-b24b-b804d5252199"
  // flavor_id       = "5d8b8337-dc22-4ac7-9d4c-fda749d364bf"
  // flavor_id       = "3481ca7b-c227-419b-bb30-0309e72286c7"
  key_pair        = "nectar-unimelb"
  security_groups = local.security_groups[each.key]

  network {
    name = "qh2-uom"
  }
  provisioner "remote-exec" {
        connection {
            type     = "ssh"
            host     = self.access_ip_v4
            user     = "ec2-user"
            private_key="${file("nectar_unimelb.pem")}"
        }

        inline = [
        "curl https://releases.rancher.com/install-docker/18.09.2.sh | sh",
        "sudo usermod -a -G docker ec2-user",
        "sudo mkdir -p /data",
        "sudo yum install -y nfs-utils",
        "sudo mount 115.146.94.139:/data /data"
        ]
  }
}
output "instances" {
  value = tomap({for node, node_v in openstack_compute_instance_v2.kubenode: node => node_v.access_ip_v4})
}
