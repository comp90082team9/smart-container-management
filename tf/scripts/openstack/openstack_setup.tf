#basics
variable "username" {
  type = string
  description = "Username used for Openstack Auth"
}
variable "password" {
  type = string
  description = "Password used for Openstack Auth"
}

variable "instance_info" {
  type = map
}

provider "openstack" {
  auth_url = "https://keystone.rc.nectar.org.au:5000/v3/"
  region = "Melbourne"
  user_name = var.username
  password = var.password
  tenant_id = "cc7aa31b9cb4406c81ff8390cb2c70d4"
  tenant_name = "unimelb-comp90082-team9"
  user_domain_name = "Default"
  project_domain_id = "default"
}