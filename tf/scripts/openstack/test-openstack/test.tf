module "test-openstack" {
    source = "../"
    username = "m.gao8@student.unimelb.edu.au"
    password = "YjMwZmJhNWI4YmNkMjhh"
    instance_info = tomap({
        "openstack-master-D-X": {
            "role" : "master",
            "az" : "default"
        },

        "openstack-worker-D-X": {
            "role" : "worker",
            "az" : "default"
        }
    })
}