locals {
  security_groups = tomap({for n, n_info in var.instance_info : n => (n_info.role == "master" ? ["${aws_security_group.kube-etcd.name}", "${aws_security_group.kube-controlplane.name}", "${aws_security_group.kube-ssh.name}"] : ["${aws_security_group.kube-worker.name}"])})
  azs = tomap({for n, n_info in var.instance_info : n => (n_info.az == "a" ? data.aws_availability_zones.available.names[0] : (n_info.az == "b" ? data.aws_availability_zones.available.names[1] : data.aws_availability_zones.available.names[2]))})
}

resource "aws_instance" "kubenode" {
  for_each = var.instance_info
  ami = "ami-03ed5bd63ba378bd8"
  instance_type = "t2.micro"
  key_name = "nectar_unimelb"
  security_groups = local.security_groups[each.key]
  # subnet_id = local.subnet_ids[each.key]
  availability_zone = local.azs[each.key]
  tags = {
    Name = "${each.key}"
  }
  provisioner "remote-exec" {
        connection {
            type     = "ssh"
            host     = self.public_ip
            user     = "ec2-user"
            private_key="${file("nectar_unimelb.pem")}"
        }

        inline = [
        "sudo yum update -y",
        "sudo amazon-linux-extras install docker -y",
        "sudo service docker start",
        "sudo usermod -a -G docker ec2-user",
        "sudo mkdir -p /data",
        "sudo yum install -y nfs-utils",
        "sudo mount 115.146.94.139:/data /data"        
        ]
  }
}
output "instances" {
  value = tomap({for node, node_v in aws_instance.kubenode: node => node_v.public_ip})
}
