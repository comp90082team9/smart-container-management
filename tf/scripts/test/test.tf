module "azure_instances" {
    source = "../azure"
    instance_info = tomap({"testnode1":{"sg":["kube-worker"],"az":"default"}, "testnode2":{"sg":["kube-worker"],"az":"default"}})
}