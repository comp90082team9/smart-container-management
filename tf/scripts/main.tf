variable "auth" {
    type = object({
        openstack = object({
            username = string
            password = string
        })
        aws = object({
            access_key = string
            secret_key = string
        })
    })
}
variable "base_cluster_info" {
    type = object({
        master_nodes = map(object({
            provider = string
            az = string
        }))
        slave_nodes = map(object({
            provider = string
            az = string
        }))
    })
}

variable "base_cluster_info_slaves_updated" {
    type = object({
        master_nodes = map(object({
            provider = string
            az = string
        }))
        slave_nodes = map(object({
            provider = string
            az = string
        }))
    })
}

variable "extra_workers" {
    type = map(object({
            provider = string
            az = string
        }))
}

variable "extra_workers_updated" {
    type = map(object({
            provider = string
            az = string
        }))
}


module "base" {
  source = "./base"
  auth = var.auth
  base_cluster_info = var.base_cluster_info
  base_cluster_info_slaves_updated = var.base_cluster_info_slaves_updated
  extra_workers = var.extra_workers
  extra_workers_updated = var.extra_workers_updated
}