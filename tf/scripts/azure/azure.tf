locals {
  security_groups = tomap({for n, n_info in var.instance_info : n => (n_info.role == "master" ? azurerm_network_security_group.kube-master.id : azurerm_network_security_group.kube-worker.id)})
}


resource "azurerm_public_ip" "publicips" {
  for_each                     = var.instance_info
  name                         = "${each.key}-publicIP"
  location                     = "${data.azurerm_resource_group.main.location}"
  resource_group_name          = "${data.azurerm_resource_group.main.name}"
  allocation_method            = "Dynamic"

}

resource "azurerm_network_interface" "nics" {
  for_each            = var.instance_info
  name                = "${each.key}-nic"
  location            = "${data.azurerm_resource_group.main.location}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  network_security_group_id = local.security_groups[each.key]

  ip_configuration {
    name                          = "kube-nic-config"
    subnet_id                     = "${azurerm_subnet.main.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.publicips[each.key].id}"
  }
}

resource "azurerm_virtual_machine" "kubenodes" {
  for_each              = var.instance_info
  name                  = "${each.key}"
  location              = "${data.azurerm_resource_group.main.location}"
  resource_group_name   = "${data.azurerm_resource_group.main.name}"
  network_interface_ids = ["${azurerm_network_interface.nics[each.key].id}"]
  vm_size               = "Standard_B2s"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "${each.key}-Disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = each.key
    admin_username = "ubuntu"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/ubuntu/.ssh/authorized_keys"
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7PMtNCS8C+Hfec3LD7zovgdxSPrNIhqkiDSlaaCBaN8TaL8/8fzkOxTuzM7sUZ4ZqQmjLuJbRCzZyZrTOEVPO+c63UmL/hGxU97Qv8S5SFItmDG1ZBEQulMs4qTJ3neEzJkiNldyDnaV3sU5FN7OviT1BZiiYG30I6cUvvcQllw2ZZNwfckJiC6QXr7KtqgXcUrG+9Kk2EZmWpP8DJO7CeYdEVZQTRVotR47GnQD0jf0VNNF/E0UDfwocSs21Zt6xLC7P4E5flYbpKvNF0OVHEkiMvB/32Zb3JNFmuQ9NXqoJqu9eF4SYY61Wg7Bo5VCJs/zrxTs85w6OUzl1oJ3N suraj@surajthakur"
    }
  }
}

data "azurerm_public_ip" "ips" {
  for_each              = var.instance_info
  name                = "${azurerm_public_ip.publicips[each.key].name}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  depends_on          = [azurerm_virtual_machine.kubenodes]
}

resource "null_resource" "cluster" {
  for_each              = var.instance_info
  triggers = {
    id = "${azurerm_virtual_machine.kubenodes[each.key].id}"
  }

  provisioner "remote-exec" {
    connection {
            type     = "ssh"
            host     = "${data.azurerm_public_ip.ips[each.key].ip_address}"
            user     = "ubuntu"
            private_key="${file("nectar_unimelb.pem")}"
        }
    
    inline = [
      "sudo apt-get update",
      "curl https://releases.rancher.com/install-docker/18.09.2.sh | sh",
      "sudo usermod -a -G docker ubuntu",
      "sudo mkdir -p /data",
      "sudo apt-get install -y nfs-common",
      "sudo mount 115.146.94.139:/data /data"
    ]
  }
  depends_on = [data.azurerm_public_ip.ips]
}

output "instances" {
  value = tomap({for node, node_v in data.azurerm_public_ip.ips: node => node_v.ip_address})
}