module "test-azure" {
    source = "../"
    instance_info = tomap({
        "azure-slave-D-2": {
            "role" : "worker",
            "az" : "default"
        }
    })
}