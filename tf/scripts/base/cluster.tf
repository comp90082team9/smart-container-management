resource "rke_cluster" "cluster" {
    # Masters
    dynamic nodes {
        for_each = local.instance_info_aws_masters
        content {
            address = module.nodes_aws.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["controlplane", "etcd"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.instance_info_azure_masters
        content {
            address = module.nodes_azure.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ubuntu"
            role    = ["controlplane", "etcd"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.instance_info_openstack_masters
        content {
            address = module.nodes_openstack.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["controlplane", "etcd"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.instance_info_gcp_masters
        content {
            address = module.nodes_gcp.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["controlplane", "etcd"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }
    # Slaves

    dynamic nodes {
        for_each = local.cluster_aws_slaves
        content {
            address = module.nodes_aws.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["worker"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.cluster_azure_slaves
        content {
            address = module.nodes_azure.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ubuntu"
            role    = ["worker"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.cluster_openstack_slaves
        content {
            address = module.nodes_openstack.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["worker"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.cluster_gcp_slaves
        content {
            address = module.nodes_gcp.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["worker"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }
    # Workers
    dynamic nodes {
        for_each = local.cluster_aws_workers
        content {
            address = module.nodes_aws.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["worker"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.cluster_azure_workers
        content {
            address = module.nodes_azure.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ubuntu"
            role    = ["worker"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.cluster_openstack_workers
        content {
            address = module.nodes_openstack.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["worker"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

    dynamic nodes {
        for_each = local.cluster_gcp_workers
        content {
            address = module.nodes_gcp.instances[nodes.key]
            hostname_override = nodes.key
            user    = "ec2-user"
            role    = ["worker"]
            ssh_key = "${file("/home/suraj/.ssh/nectar_unimelb")}"
        }
    }

  addons_include = [
    "https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml",
    "https://gist.githubusercontent.com/superseb/499f2caa2637c404af41cfb7e5f4a938/raw/930841ac00653fdff8beca61dab9a20bb8983782/k8s-dashboard-user.yml",
  ]
  depends_on = [module.nodes_aws, module.nodes_azure, module.nodes_openstack, module.nodes_gcp]
}

resource "local_file" "kube_cluster_yaml" {
  filename = format("%s/%s" , path.root, "kube_config_cluster.yml")
  content = rke_cluster.cluster.kube_config_yaml
}
