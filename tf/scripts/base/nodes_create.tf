locals {
    # aws
    instance_info_aws_masters = tomap({for n, n_v in var.base_cluster_info.master_nodes : n => {"role": "master","az": n_v.az} if n_v.provider == "aws"})
    instance_info_aws_slaves = tomap({for n, n_v in var.base_cluster_info.slave_nodes : n => {"role": "worker","az": n_v.az} if n_v.provider == "aws"})
    instance_info_aws_workers = tomap({for n, n_v in var.extra_workers : n => {"role": "worker","az": n_v.az} if n_v.provider == "aws"})

    # azure
    instance_info_azure_masters = tomap({for n, n_v in var.base_cluster_info.master_nodes : n => {"role": "master","az": n_v.az} if n_v.provider == "azure"})
    instance_info_azure_slaves = tomap({for n, n_v in var.base_cluster_info.slave_nodes : n => {"role": "worker","az": n_v.az} if n_v.provider == "azure"})
    instance_info_azure_workers = tomap({for n, n_v in var.extra_workers : n => {"role": "worker","az": n_v.az} if n_v.provider == "azure"})

    # openstack
    instance_info_openstack_masters = tomap({for n, n_v in var.base_cluster_info.master_nodes : n => {"role": "master","az": n_v.az} if n_v.provider == "openstack"})
    instance_info_openstack_slaves = tomap({for n, n_v in var.base_cluster_info.slave_nodes : n => {"role": "worker","az": n_v.az} if n_v.provider == "openstack"})
    instance_info_openstack_workers = tomap({for n, n_v in var.extra_workers : n => {"role": "worker","az": n_v.az} if n_v.provider == "openstack"})

    # gcp
    instance_info_gcp_masters = tomap({for n, n_v in var.base_cluster_info.master_nodes : n => {"role": "master","az": n_v.az} if n_v.provider == "gcp"})
    instance_info_gcp_slaves = tomap({for n, n_v in var.base_cluster_info.slave_nodes : n => {"role": "worker","az": n_v.az} if n_v.provider == "gcp"})
    instance_info_gcp_workers = tomap({for n, n_v in var.extra_workers : n => {"role": "worker","az": n_v.az} if n_v.provider == "gcp"})

    # cluster nodes
    cluster_aws_slaves = tomap({for n, n_v in var.base_cluster_info.slave_nodes : n => {"role": "worker","az": n_v.az} if n_v.provider == "aws" && contains(keys(var.base_cluster_info_slaves_updated.slave_nodes),n)})
    cluster_aws_workers = tomap({for n, n_v in var.extra_workers : n => {"role": "worker","az": n_v.az} if n_v.provider == "aws" && contains(keys(var.extra_workers_updated),n)})

    cluster_azure_slaves = tomap({for n, n_v in var.base_cluster_info.slave_nodes : n => {"role": "worker","az": n_v.az} if n_v.provider == "azure" && contains(keys(var.base_cluster_info_slaves_updated.slave_nodes),n)})
    cluster_azure_workers = tomap({for n, n_v in var.extra_workers : n => {"role": "worker","az": n_v.az} if n_v.provider == "azure" && contains(keys(var.extra_workers_updated),n)})
   
    cluster_openstack_slaves = tomap({for n, n_v in var.base_cluster_info.slave_nodes : n => {"role": "worker","az": n_v.az} if n_v.provider == "openstack" && contains(keys(var.base_cluster_info_slaves_updated.slave_nodes),n)})
    cluster_openstack_workers = tomap({for n, n_v in var.extra_workers : n => {"role": "worker","az": n_v.az} if n_v.provider == "openstack" && contains(keys(var.extra_workers_updated),n)})
   
    cluster_gcp_slaves = tomap({for n, n_v in var.base_cluster_info.slave_nodes : n => {"role": "worker","az": n_v.az} if n_v.provider == "gcp" && contains(keys(var.base_cluster_info_slaves_updated.slave_nodes),n)})
    cluster_gcp_workers = tomap({for n, n_v in var.extra_workers : n => {"role": "worker","az": n_v.az} if n_v.provider == "gcp" && contains(keys(var.extra_workers_updated),n)})
   
}
module "nodes_aws" {
    source = "../aws"
    instance_info = merge(local.instance_info_aws_masters,local.instance_info_aws_slaves,local.instance_info_aws_workers)
    access_key = var.auth.aws.access_key
    secret_key = var.auth.aws.secret_key
}

module "nodes_azure" {
    source = "../azure"
    instance_info = merge(local.instance_info_azure_masters, local.instance_info_azure_slaves, local.instance_info_azure_workers)
}

module "nodes_openstack" {
    source = "../openstack"
    instance_info = merge(local.instance_info_openstack_masters, local.instance_info_openstack_slaves, local.instance_info_openstack_workers)
    username = var.auth.openstack.username
    password = var.auth.openstack.password
}

module "nodes_gcp" {
    source = "../gcp"
    instance_info = merge(local.instance_info_gcp_masters, local.instance_info_gcp_slaves, local.instance_info_gcp_workers)
}