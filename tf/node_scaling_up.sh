#!/bin/bash

# Example usage: ./node_scaling_up.sh -u ./config/updated_cluster.tfvars.json
usage() { echo "Usage: $0 [-u ./config/updated_cluster.tfvars.json]" 1>&2; exit 1; }

while getopts ":u:" o; do
    case "${o}" in
        u)
            u=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${u}" ]; then
    usage
fi

# add node to cluster first
cd scripts
jq '. + { "base_cluster_info"} | ."base_cluster_info"=."base_cluster_info_slaves_updated"' ../${u} > ./temp/temp1.tfvars.json
jq '. + { "extra_workers"} | ."extra_workers"=."extra_workers_updated"' ./temp/temp1.tfvars.json > ./temp/temp.tfvars.json
rm ./temp/temp1.tfvars.json
terraform apply -var-file=./auth.tfvars -var-file=./temp/temp.tfvars.json -auto-approve

# updated cluster
jq 'del(.base_cluster_info_slaves_updated) | del(.extra_workers_updated)' ./temp/temp.tfvars.json > ./base_cluster.tfvars.json
exit 0