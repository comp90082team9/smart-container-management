#!/bin/bash

# Example usage: ./node_scaling_down.sh -u ./config/updated_cluster.tfvars.json
usage() { echo "Usage: $0 [-u ./config/updated_cluster.tfvars.json]" 1>&2; exit 1; }

while getopts ":u:" o; do
    case "${o}" in
        u)
            u=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${u}" ]; then
    usage
fi
cd scripts

# # check if a base_cluster exist
# if [ -f base_cluster.tfvars.json ]; then
#     # previous_workers exist
#     # jq -s '.[0] * .[1]' ./previous_extra_workers.tfvars.json ./temp/temp2.tfvars.json > ./temp/temp3.tfvars.json
#     echo "base cluster exist"
# else
#     # previous_worker not exist
#     echo "previous extra nodes not exist, meaning scaling down the worker nodes in the base cluster"    
# fi


# remove worker from cluster
terraform apply -var-file=./auth.tfvars -var-file=./base_cluster.tfvars.json -var-file="../${u}" -auto-approve
# delete node
jq '. + { "base_cluster_info"} | ."base_cluster_info"=."base_cluster_info_slaves_updated"' ../${u} > ./temp/temp1.tfvars.json
jq '. + { "extra_workers"} | ."extra_workers"=."extra_workers_updated"' ./temp/temp1.tfvars.json > ./temp/temp.tfvars.json
rm ./temp/temp1.tfvars.json
terraform apply -var-file=./auth.tfvars -var-file=./temp/temp.tfvars.json -auto-approve
# updated cluster
jq 'del(."base_cluster_info_slaves_updated") | del(."extra_workers_updated")' ./temp/temp.tfvars.json > base_cluster.tfvars.json
exit 0