#!/bin/bash

# Example usage: ./cluster_set_up.sh -b base_cluster.tfvars.json
usage() { echo "Usage: $0 [-b ./config/base_cluster.tfvars.json]" 1>&2; exit 1; }

while getopts ":b:" o; do
    case "${o}" in
        b)
            b=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${b}" ]; then
    usage
fi
cd ./scripts
terraform init
jq '. + { "base_cluster_info_slaves_updated"} | ."base_cluster_info_slaves_updated"=."base_cluster_info"' ../${b} > ./temp/temp1.tfvars.json
jq '. + { "extra_workers_updated"} | ."extra_workers_updated"=."extra_workers"' ./temp/temp1.tfvars.json > ./temp/temp.tfvars.json
rm ./temp/temp1.tfvars.json
terraform apply -var-file=./auth.tfvars -var-file=./temp/temp.tfvars.json -auto-approve
cp ../${b} ./base_cluster.tfvars.json
cat ./kube_config_cluster.yml > ~/.kube/config
kubectl get nodes
exit 0
