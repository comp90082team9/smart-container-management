#!/usr/bin/env python
# coding: utf-8

import time
import requests
import json
import copy
import re
import subprocess
import pandas as pd
from sys import argv


def check():
    scaleup_url = "http://115.146.94.151:5000/monitoring/scaleup"
    response = requests.get(scaleup_url)
    response_scaleup = response.json()

    if (response_scaleup["status"] == "ok"):
        print ("scaling up: ok")
        # with open('text_server_2.txt', 'w') as outfile:
        #     outfile.write("scaling up: ok\n")

    #     # no need to scaling up, continuing checking if needs to scaling down

        scaledown_url = "http://115.146.94.151:5000/monitoring/scaledown"
        response = requests.get(scaledown_url)
        response_scaledown = response.json()
        # print(type(response_scaledown))
        
        if (response_scaledown["status"] == "ok"):
            print ("scaling down: ok")
            # with open('text_server_2.txt', 'w') as outfile:
            #     outfile.write("scaling down: ok")
            return 10
        elif (response_scaledown["status"] == "normal"):
            print ("scaling down: normal")
            # with open('text_server_2.txt', 'w') as outfile:
            #     outfile.write("scaling down: normal")
            return 20
        elif (response_scaledown["status"] == "high"):
            print ("scaling down: high")
            # with open('text_server_2.txt', 'w') as outfile:
            #     outfile.write("scaling down: high")
            return 30
        elif (response_scaledown["status"] == "down"):
            print ("scaling down: " + response_scaledown["content"])
            # with open('text_server_2.txt', 'w') as outfile:
            #     outfile.write("scaling down: " + response_scaledown["content"])
            with open("./scripts/base_cluster.tfvars.json") as base_cluster_file:
                base_cluster = json.load(base_cluster_file)
                updated_cluster = copy.deepcopy(base_cluster)
                del updated_cluster["extra_workers"][response_scaledown["content"]]
                updated_cluster["base_cluster_info_slaves_updated"] = updated_cluster["base_cluster_info"]
                updated_cluster["extra_workers_updated"] = updated_cluster["extra_workers"]
                del updated_cluster["extra_workers"]
                del updated_cluster["base_cluster_info"]
                with open("./config/updated_cluster.tfvars.json", "w") as update_cluster_file:
                    update_cluster_file.write(json.dumps(updated_cluster, indent=2))
            p = subprocess.Popen(["./node_scaling_down.sh", "-u", "./config/updated_cluster.tfvars.json"])
            p.wait()
            print ("scaling down: success")
            # with open('text_server_2.txt', 'w') as outfile:
            #     outfile.write("scaling down: success")
            return 30
        else:
            print ("Unknown error: " + response_scaledown)
            # with open('text_server_2.txt', 'w') as outfile:
            #     outfile.write("Unknown error: " + response_scaledown)   
        
    elif (response_scaleup["status"] == "up"):
        # with open("node_status.txt", "r") as myfile:
        #     nodes_info = myfile.read()      
        nodes_info = subprocess.check_output(["kubectl","get","nodes"])
        node_info_str = str(nodes_info)
        if (response_scaleup["provider"] == "aws"):
            info = "aws-" + response_scaleup["az"]
            detail = dict()
            detail["provider"] = "aws"
            detail["az"] = response_scaleup["az"]
        elif (response_scaleup["provider"] == "azure"):
            info = "azure-x"
            detail = dict()
            detail["provider"] = "azure"
            detail["az"] = "default"
        elif (response_scaleup["provider"] == "openstack"):
            info = "openstack-x"
            detail = dict()
            detail["provider"] = "openstack"
            detail["az"] = "default"
        elif (response_scaleup["provider"] == "gcp"):
            info = "gcp-" + response_scaleup["az"]
            detail = dict()
            detail["provider"] = "gcp"
            detail["az"] = response_scaleup["az"]

        nodes = re.findall("worker-" + info + "-\d+", node_info_str)
        if (len(nodes)) > 0:
            name_index = dict()
            for node in nodes:
                name_index[node] = int(node.split("-")[-1])
            new_node_index = max(name_index.values()) + 1
        else:
            new_node_index = 1
        new_node_name = "worker-" + info + "-" +str(new_node_index)
        print ("scaling up: " + new_node_name)
        # with open('text_server_2.txt', 'w') as outfile:
        #     outfile.write("scaling up: " + new_node_name)   

        with open("./scripts/base_cluster.tfvars.json") as base_cluster_file:
            base_cluster = json.load(base_cluster_file)
            updated_cluster = copy.deepcopy(base_cluster)
            updated_cluster["extra_workers"][new_node_name] = detail
            updated_cluster["base_cluster_info_slaves_updated"] = updated_cluster["base_cluster_info"]
            updated_cluster["extra_workers_updated"] = updated_cluster["extra_workers"]
            del updated_cluster["extra_workers"]
            del updated_cluster["base_cluster_info"]
            with open("./config/updated_cluster.tfvars.json", "w") as update_cluster_file:
                update_cluster_file.write(json.dumps(updated_cluster, indent=2))
            p = subprocess.Popen(["./node_scaling_up.sh", "-u", "./config/updated_cluster.tfvars.json"])
            p.wait()
            print ("scaling down: success")
        return 30
    else:
        print ("Unknown error " + response_scaleup)
        # with open('text_server_2.txt', 'w') as outfile:
        #     outfile.write("Unknown error " + response_scaleup)  


def main():
    while(True):
        waiting_time = check()
        time.sleep(waiting_time)
#python3 server.py http://127.0.0.1:5000
main()

