const express = require('express');
const app = express();
const port = 8000;
const cors = require('cors');
app.use(cors());
app.use(express.json());
var http = require('http');
var fs = require('fs');
const path = require('path');

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '.', 'app', 'UI', 'index2.html'));
});

app.get('/dashboard', function (req, res) {
    //res.send('Working!');
    res.sendFile(path.join(__dirname, '.', 'app', 'UI', 'Dashboard2.html'));
});

app.get('/css', function (req, res) {
    //res.send('Working!');
    res.sendFile(path.join(__dirname, '.', 'app', 'UI', 'dashboard.css'));
});

/**Reads file with projected coordinates, converts to required coordinates and then sends back apt file */
app.get('/input', function (req, res) {
    var numPoints = req.query.points;
    var req = require('reproject');
    var inFile = "";
    var firstProj = '+proj=utm +zone=50 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'
    var secondProj = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs';
    console.log(numPoints);
    if(numPoints==5){
       inFile="Rndm5ptsProjected.json";
    }
    if(numPoints==100){
        inFile="Rndm100ptsProjected.json";
    }
    if(numPoints==1000){
        inFile="Rndm1000ptsProjected.json";
    }
    let rawPoints = fs.readFileSync(path.join(__dirname, '.', 'app', 'data', inFile));
    let polyPoints = JSON.parse(rawPoints)
    var reprojection = req.reproject(polyPoints, firstProj, secondProj)
    reprojection = JSON.stringify(reprojection)
    let obj = JSON.parse(reprojection);
    res.json(obj);
});

app.get('/result', (req, res) => {
    var numPoints = req.query.points;
    var req = require('reproject');
    var file = "";
    var firstProj = '+proj=utm +zone=50 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'
    var secondProj = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs';
    console.log(numPoints);
    // const homedir = require('os').homedir();
    const homedir = "/data";

    const timeout = setInterval(function() {
        //Add file based on num of points _100pts
        if(numPoints==5) {
            file = path.join(homedir, 'output', 'ZScoreOMSTest.geojson');
        }
        else if(numPoints==100) {
            file = path.join(homedir, 'output', 'ZScoreOMSTest_100pts.geojson');
        }
        else if(numPoints==1000) {
            file = path.join(homedir, 'output', 'ZScoreOMSTest_1000pts.geojson');
        }
        const fileExists = fs.existsSync(file);

        console.log('Checking for: ', file);
        console.log('Exists: ', fileExists);

        if (fileExists) {
            clearInterval(timeout);
            let rawPoints = fs.readFileSync(file);
            let polyPoints = JSON.parse(rawPoints)
            var reprojection = req.reproject(polyPoints, firstProj, secondProj)
            reprojection = JSON.stringify(reprojection)
            let obj = JSON.parse(reprojection);
            res.json(obj);
        }
    }, 5000);
});

app.get('/delete-deployments', (req, res) => {
        const {KubeConfig, Client} = require('kubernetes-client');
        const kubeconfig = new KubeConfig();
        kubeconfig.loadFromFile('config');
        const Request = require('kubernetes-client/backends/request');
        const backend = new Request({kubeconfig});
        const client = new Client({backend, version: '1.13'});

        const removed = async function() { 
            var deployments = [path.join(__dirname, '.', 'app', 'data', 'connectivity-deploy.json'), path.join(__dirname, '.', 'app', 'data', 'density-deploy.json'), path.join(__dirname, '.', 'app', 'data', 'lum-deploy.json'), path.join(__dirname, '.', 'app', 'data', 'polygon-deploy.json')];
            for(var i=0; i<4; i++) {
                const deploymentManifest = require(deployments[i]);
                await client.apis.apps.v1.namespaces('default').deployments(deploymentManifest.metadata.name).delete();
                console.log('Deployment deleted!');
            }
        }
        removed().catch((err)=> {
            console.log(err);
        });
        res.status(200).send({status: 'OK'});
});

app.get('/delete-pods', (req, res) => {
    const {KubeConfig, Client} = require('kubernetes-client');
    const kubeconfig = new KubeConfig();
    kubeconfig.loadFromFile('config');
    const Request = require('kubernetes-client/backends/request');
    const backend = new Request({kubeconfig});
    const client = new Client({backend, version: '1.13'});

    const removedPods = async function() { 
        var pods = [path.join(__dirname, '.', 'app', 'data', 'sendpoints-pod.json'), path.join(__dirname, '.', 'app', 'data', 'sendzscore-pod.json')];
        for(var i=0; i<2; i++) {
            const podManifest = require(pods[i]);
            await client.api.v1.namespaces('default').pods(podManifest.metadata.name).delete();
            console.log('Pod deleted!');
        }
    }
    removedPods().catch((err)=> {
        console.log(err);
    });
    res.status(200).send({status: 'OK'});
});

 //CONVERT YAML TO JSON
app.get('/convert', (req, res)=>{
    var input = 'sendzscore-pod.yaml';
    var output = 'sendzscore-pod.json';
    var yaml = require('js-yaml');
    var fs = require('fs');
    var obj = yaml.load(fs.readFileSync(path.join(__dirname, '.', 'app', 'data', input), {encoding: 'utf-8'}));
    fs.writeFileSync(path.join(__dirname, '.', 'app', 'data', output), JSON.stringify(obj, null, 2)); 
    res.status(200).send({status: 'OK'});
});

require('./app/routes')(app);

http.createServer({}, app).listen(port, ()=>{console.log('Live on port: ' + port);});

