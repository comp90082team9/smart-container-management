module.exports = function (app) {
    app.get('/points', (req, res) => {
        var path = require('path');
        var fs = require('fs');

        var numPoints = req.query.num;
        console.log(numPoints, 22);
        res.status(200).send({ status: 'OK' });
        const { KubeConfig, Client } = require('kubernetes-client');
        const kubeconfig = new KubeConfig();
        kubeconfig.loadFromFile('config');
        const Request = require('kubernetes-client/backends/request');
        const backend = new Request({ kubeconfig });
        const client = new Client({ backend, version: '1.13' });

        //Deploy according to number of points
        const deploy = async function() {
            var deployments = [path.join(__dirname, '..', 'data', 'connectivity-deploy.json'), path.join(__dirname, '..', 'data', 'density-deploy.json'), path.join(__dirname, '..', 'data', 'lum-deploy.json'), path.join(__dirname, '..', 'data', 'polygon-deploy.json')];
            for(var i=0; i<4; i++) {
                var deploymentManifest = require(deployments[i]);
                await client.apis.apps.v1.namespaces('default').deployments(deploymentManifest.metadata.name).delete();
                console.log('Deployment deleted!');
                var createdDeployment = await client.apis.apps.v1.namespaces('default').deployments.post({ body: deploymentManifest });
                console.log('Deployment', i, ' created:', createdDeployment);
            }
        }   

        //Deploy pods according to number of points
        const podDeploy = async function () {
            var c=[];
            var pods = [path.join(__dirname, '..', 'data', 'sendpoints-pod.json'), path.join(__dirname, '..', 'data', 'sendzscore-pod.json')];
            var podExist = await client.api.v1.namespaces('default').pods.get();
            for (var i = 0; i < 2; i++) {
                var podManifest = require(pods[i]);
                if(numPoints==100||numPoints==1000) {
                    await client.api.v1.namespaces('default').pods(podManifest.metadata.name).delete();
                }
                for (var j = 0; j < podExist.body.items.length; j++) {
                    c.push(podExist.body.items[j].metadata.name);
                }
                if (c.includes(podManifest.metadata.name)) {
                    var status = await client.api.v1.namespaces('default').pods(podManifest.metadata.name).status.get().then(stat => { return stat.body.status.phase });
                    console.log(3);
                    if (status !== 'Running') {
                        console.log(4);
                        await client.api.v1.namespaces('default').pods(podManifest.metadata.name).delete();
                        var createdPod = await client.api.v1.namespaces('default').pods.post({ body: podManifest });
                        console.log('Pod', i, ' created:', createdPod);
                    }
                }
                else {
                    var createdPod = await client.api.v1.namespaces('default').pods.post({ body: podManifest });
                    console.log('Pod', i, ' created:', createdPod);
                }
            }
        }
        if(numPoints==100 || numPoints==1000){
            deploy().catch((err)=>{
                console.log(err);
            });
        }
       
        podDeploy().catch((err) => {
            console.log(err);
        });

        //res.redirect('/output');
    });
};